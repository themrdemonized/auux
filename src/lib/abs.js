export default function (value) {
    return (value >= 0 ? value : -value);
}